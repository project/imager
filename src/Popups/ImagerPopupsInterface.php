<?php
/**
 * @file
 * Contains \Drupal\imager\Popups\ImagerPopupsInterface
 */

namespace Drupal\imager\Popups;


interface ImagerPopupsInterface {
  static public function build(array $config);
}   
